//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bcrypt = require('bcrypt');
var saltRounds = 10;
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-Width, Content-Type, Accept");
  next();
})
const config = require('./config')
const apipubCtrl = require('./apipub/apipub')

var requestjson = require('request-json');

var path = require('path');

var urlMLabRaiz = "https://api.mlab.com/api/1/databases/adominguez/collections";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
/*
app.get('/', function(req, res) {
  //res.send("HOla Mundo desde NODE.JS");
  res.sendFile(path.join(__dirname, 'index.html'));
})
*/

//Peticion REST para Login
app.post('/Login', function(req, res) {
 res.header("Access-Control-Allow-Origin", "*");
 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
 var email = req.headers.email;
 var password = req.headers.password;
 var query = 'q={"email":"'+ email + '"}';
 console.log(query);
 clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query);
 console.log(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query);
 clienteMLabRaiz.get('', function(err, resM, body) {
   if (!err) {
     if (body.length > 0) { //Login ok
      console.log('pass ' + req.headers.password + ' ' + body[0].password);
      bcrypt.compare(req.headers.password, body[0].password, function(err, ok) {
        if (ok) {
          res.status(200).send('Usuario logado correctamente');
        } else {
          res.status(404).send('Password no encontrado');
        }
       });
     } else {
      res.status(404).send('Usuario no encontrado');
     }
   } else {
     console.log(body);
   }
 })
 })


 //Inserta en Collection Usuarios
 app.post('/GuardaUser', function(req, res) {
   clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Usuarios?" + apiKey);
   //console.log(urlMLabRaiz + "/Usuarios?" + apiKey);
   //var email = req.body.email;
   //var password = req.body.password;
   bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
     req.body.password = hash;
     //console.log('ENCIP ' + req.body.password);
     //console.log('ENCIP2 ' + req.body.email + req.body.password + req.body.nombre);
     clienteMLabRaiz.post('', req.body, function(err,resM, body) {
        if (err) {
          console.log(body); }
        else {
          res.send(body);
        }
     })
    });
    //console.log('ENCIP3 ' + req.body.email + req.body.password + req.body.nombre);
 })

 //Inserta inversiones de Fondos
 app.post('/Invierte', function(req, res) {
   //res.header("Access-Control-Allow-Origin", "*");
   //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Inversion?" + apiKey);
   console.log(urlMLabRaiz + "/Inversion?" + apiKey);
   clienteMLabRaiz.post('', req.body, function(err,resM, body) {
     if (err) {
       console.log(body); }
     else {
       res.send(body);
     }
   })
 })

 //Peticion GET para obtener el saldo
 app.get('/Saldo', function(req, res) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  var email = req.headers.email;
  var query = 'q={"email":"'+ email + '"}';
  console.log(query);
  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Inversion?" + apiKey + "&" + query);
  console.log(urlMLabRaiz + "/Inversion?" + apiKey + "&" + query);
  clienteMLabRaiz.get('', function(err, resM, body) {
  if (err) {
    console.log(body); }
  else {
    res.send(body);
    }
  })
  })

  app.get('/apipubget',function(req,res) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-Width, Content-Type, Accept");
   //var now = new Date().toISOString().split('T')[0]
   var now = new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString().split('T')[0]
   var vserie_TC = config.serie_TC;
   var vserie_TIIE = config.serie_TIIE;
   //now.setDate(now.getDate() - 1);
   //var serieBancoMex = config.url_bmx +'/'+ vserie_TC +',' + vserie_TIIE +'/datos/2019-10-02/2019-10-02';
   var serieBancoMex = config.url_bmx +'/'+ vserie_TC +',' + vserie_TIIE +'/datos/' + now + '/' + now;
   console.log("fecha "+ now + serieBancoMex);
   apipubCtrl.getTipoCam(serieBancoMex)
   .then( data => res.send(data)) // Result from the `response.json()` call
   .catch(error => console.error(error))
    })


/*
 //Inserta inversiones de Fondos
 app.post('/Invierte', function(req, res) {
  //res.header("Access-Control-Allow-Origin", "*");
  //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  var email = req.headers.email;
  var query = 'q={"email":"'+ email + '"}';
  console.log(query);
  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Inversion?" + apiKey + "&" + query);
  console.log(urlMLabRaiz + "/Fondos?" + apiKey + "&" + query);
  clienteMLabRaiz.put('', req.body, function(err,resM, body) {
    if (err) {
      console.log(body); }
    else {
      res.send(body);
    }
  })
  })
  */
